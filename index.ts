import createApp from './utils/app'
import config from './utils/config'

console.log(config)

const app = createApp()
app.listen(config.PORT, config.HOST, () => console.log(`listening at http://${config.HOST}:${config.PORT}`))

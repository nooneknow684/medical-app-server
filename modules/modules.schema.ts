import { z } from "zod";

export const preprocessString = (data: any) => {
    const res = parseInt(data)
    return isNaN(res) ? undefined : res
}

export const preprocessDate = (data: any) => {
    return new Date(String(data))
}

export const paginationQuerySchema = z.object({
    page: z.preprocess(preprocessString, z.number().positive().default(1),),
    per_page: z.preprocess(preprocessString, z.number().positive().default(10),),
})

import { Router } from 'express'
import { validateBody, validateParam, validateQuery } from '../../utils/helper'
import { createMedicineHandler, deleteMedicineHandler, getAllMedicinesHandler, getMedicineHandler, restoreMedicineHandler, updateMedicineHandler } from './medicine.controller'
import { createMedicineBodySchema, getAllMedicineQuerySchema, medicineDetailDeletedParamSchema, medicineDetailParamSchema, updateMedicineBodySchema } from './medicine.schema'

const medicineRoute = Router()

medicineRoute.route('/')
    .get([validateQuery(getAllMedicineQuerySchema)], getAllMedicinesHandler)
    .post([validateBody(createMedicineBodySchema)], createMedicineHandler)

medicineRoute.route('/:medicine').all([validateParam(medicineDetailParamSchema)])
    .get(getMedicineHandler)
    .put([validateBody(updateMedicineBodySchema)], updateMedicineHandler)
    .delete([], deleteMedicineHandler)

medicineRoute.post('/:medicine/restore', [validateParam(medicineDetailDeletedParamSchema)], restoreMedicineHandler)


export default medicineRoute

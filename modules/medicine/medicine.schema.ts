import { z } from "zod";
import { paginationQuerySchema, preprocessString } from "../modules.schema";
import { medicineExists } from "../modules.validation";

export const getAllMedicineQuerySchema = paginationQuerySchema.extend({
    name: z.string().optional(),
    sort: z.enum(['name', '-name']).default('name'),

    deleted: z.preprocess((val) => val != undefined, z.boolean().default(false))
})

export const medicineDetailParamSchema = z.object({
    medicine: z.preprocess(preprocessString, z.number().refine(medicineExists.validator(), medicineExists.message))
})

export const medicineDetailDeletedParamSchema = z.object({
    medicine: z.preprocess(preprocessString, z.number().refine(medicineExists.validator(true), medicineExists.message))
})

export const createMedicineBodySchema = z.object({
    name: z.string(),
})

export const updateMedicineBodySchema = z.object({
    name: z.string().optional()
})


export type GetAllMedicineQueryType = z.infer<typeof getAllMedicineQuerySchema>

export type MedicineDetailParamType = z.infer<typeof medicineDetailParamSchema>
export type CreateMedicineBodyType = z.infer<typeof createMedicineBodySchema>
export type UpdateMedicineBodyType = z.infer<typeof updateMedicineBodySchema>

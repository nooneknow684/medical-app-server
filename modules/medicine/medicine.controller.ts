import { Response } from 'express'
import { paginateResponse, TypedRequest } from '../../utils/helper'

import { CreateMedicineBodyType, GetAllMedicineQueryType, MedicineDetailParamType, UpdateMedicineBodyType } from './medicine.schema'
import { createMedicine, deleteMedicine, getAllMedicines, getMedicine, restoreMedicine, updateMedicine } from './medicine.service'

export async function getAllMedicinesHandler(req: TypedRequest<any, any, GetAllMedicineQueryType>, res: Response) {
    const [count, medicines] = await getAllMedicines(req.query)
    return res.status(200).json(paginateResponse(req.query, count, medicines))
}
export async function getMedicineHandler(req: TypedRequest<any, MedicineDetailParamType>, res: Response) {
    const medicine = await getMedicine(req.params)
    return res.status(200).json(medicine)
}
export async function createMedicineHandler(req: TypedRequest<CreateMedicineBodyType>, res: Response) {
    const createdMedicine = await createMedicine(req.body)
    return res.status(201).json(createdMedicine)
}
export async function updateMedicineHandler(req: TypedRequest<UpdateMedicineBodyType, MedicineDetailParamType>, res: Response) {
    const updatedMedicine = await updateMedicine(req.params, req.body)
    return res.status(200).json(updatedMedicine)
}
export async function deleteMedicineHandler(req: TypedRequest<any, MedicineDetailParamType>, res: Response) {
    const deletedMedicine = await deleteMedicine(req.params)
    return res.status(200).json(deletedMedicine)
}
export async function restoreMedicineHandler(req:TypedRequest<any,MedicineDetailParamType>,res:Response){
    const restoredMedicine = await restoreMedicine(req.params)
    return res.status(200).json(restoredMedicine)
}

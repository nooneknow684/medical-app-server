import { Prisma } from "@prisma/client";

import prisma from "../../utils/prisma";
import { CreateMedicineBodyType, GetAllMedicineQueryType, MedicineDetailParamType, UpdateMedicineBodyType } from "./medicine.schema";

const defaultSelect: Prisma.MedicineSelect = {
    id: true,
    name: true,
    created_at: true,
    deleted_at: true,
    updated_at: true,
    qty_detail: {
        select: {
            id: true,
            qty: true,
            unit: {
                select: {
                    id: true,
                    name: true,
                    color: true,
                    icon: true
                }
            }
        },
        where: {
            unit: {
                deleted_at: null
            }
        }
    }
}

export async function getAllMedicines(query: GetAllMedicineQueryType) {
    const filter: Prisma.MedicineWhereInput = {
        deleted_at: null,
        name: {
            startsWith: query.name
        },
    }

    const orderBy: Prisma.MedicineOrderByWithRelationInput = {
        name: query.sort == 'name' ? 'asc' : query.sort == '-name' ? 'desc' : undefined
    }

    return await prisma.$transaction([
        prisma.medicine.count({ where: filter }),
        prisma.medicine.findMany({ where: filter, orderBy: orderBy, select: defaultSelect, take: query.per_page, skip: (query.page - 1) * query.per_page })
    ])
}

export async function getMedicine(param: MedicineDetailParamType) {
    return await prisma.medicine.findFirst({ select: { ...defaultSelect }, where: { deleted_at: null, id: param.medicine } })
}

export async function createMedicine(data: CreateMedicineBodyType) {
    return await prisma.medicine.create({ select: { ...defaultSelect }, data })
}

export async function updateMedicine(param: MedicineDetailParamType, data: UpdateMedicineBodyType) {
    const updatedMedicine = await prisma.medicine.update({ where: { id: param.medicine }, data, select: defaultSelect })
    return updatedMedicine
}

export async function deleteMedicine(param: MedicineDetailParamType) {
    const deletedMedicine = await prisma.medicine.update({ where: { id: param.medicine }, data: { deleted_at: new Date() }, select: defaultSelect })
    return deletedMedicine
}

export async function restoreMedicine(param: MedicineDetailParamType) {
    const restoredMedicine = await prisma.medicine.update({ where: { id: param.medicine }, data: { deleted_at: null }, select: defaultSelect })
    return restoredMedicine
}

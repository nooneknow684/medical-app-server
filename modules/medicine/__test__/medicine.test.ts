import supertest from "supertest"
import createApp from "../../../utils/app"
import prisma from "../../../utils/prisma"
import { clearDatabase } from "../../../__test__/before-test"
import { mockMedicine } from "../../../__test__/factory"

const app = createApp()

describe("Test medicine routes", () => {
    beforeAll(async () => {
        await clearDatabase()
        await mockMedicine()
    })

    describe("Get all medicine route", () => {
        it("Get all medicine", async () => {
            const { body, statusCode } = await supertest(app).get("/medicine")
            expect(statusCode).toBe(200)
            expect(body.count).toBe(87)
            expect(body.data).toHaveLength(10)
        })
        it("Get all medicine ordered by name desc", async () => {
            const { body, statusCode } = await supertest(app).get("/medicine").query({ sort: '-name' })
            expect(statusCode).toBe(200)
            expect(body.count).toBe(87)
            expect(body.data).toHaveLength(10)
        })
        it("Get all medicine ordered by paginated as 15", async () => {
            const { body, statusCode } = await supertest(app).get("/medicine").query({ per_page: 15, page: 2 })
            expect(statusCode).toBe(200)
            expect(body.count).toBe(87)
            expect(body.data).toHaveLength(15)
        })
        it("Get all medicine ordered by paginated as 15 at last page", async () => {
            const { body, statusCode } = await supertest(app).get("/medicine").query({ per_page: 15, page: 6 })
            expect(statusCode).toBe(200)
            expect(body.count).toBe(87)
            expect(body.data).toHaveLength(12)
        })
        it("Get all medicine ordered by paginated as 15 exceeding page", async () => {
            const { body, statusCode } = await supertest(app).get("/medicine").query({ per_page: 15, page: 8 })
            expect(statusCode).toBe(200)
            expect(body.count).toBe(87)
            expect(body.data).toHaveLength(0)
        })
        it("Get all medicine start with Pa", async () => {
            const { body, statusCode } = await supertest(app).get("/medicine").query({ name: 'pa' })
            const count = await prisma.medicine.count({ where: { name: { startsWith: 'pa' } } })
            expect(statusCode).toBe(200)
            expect(body.count).toBe(count)
            expect(body.data).toHaveLength(Math.min(count, 10))
        })
    })

    describe("Get medicine route", () => {
        it("Get non-existing medicine : 404", async () => {
            const { statusCode } = await supertest(app).get("/medicine/200")
            expect(statusCode).toBe(404)
        })
        it("Get deleted medicine : 404", async () => {
            const { statusCode } = await supertest(app).get("/medicine/109")
            expect(statusCode).toBe(404)
        })
        it("Get medicine : 200", async () => {
            const { statusCode } = await supertest(app).get("/medicine/8")
            expect(statusCode).toBe(200)
        })
    })

    describe("Create medicine route", () => {
        it("Create medicine without name : 400", async () => {
            const { statusCode } = await supertest(app).post("/medicine")
            expect(statusCode).toBe(400)
        })
        it("Create medicine : 201", async () => {
            const { statusCode, body } = await supertest(app)
                .post("/medicine")
                .send({
                    name: "Estrogen"
                })
            expect(statusCode).toBe(201)
            expect(body.name).toBe("Estrogen")
        })
    })

    describe("Update medicine route", () => {
        it("Update non-existing medicine : 404", async () => {
            const { statusCode } = await supertest(app).put("/medicine/1000")
            expect(statusCode).toBe(404)
        })
        it("Update deleted medicine : 404", async () => {
            const { statusCode } = await supertest(app).put("/medicine/100")
            expect(statusCode).toBe(404)
        })
        it("Update medicine without name : 200", async () => {
            const { statusCode } = await supertest(app).put("/medicine/1")
            expect(statusCode).toBe(200)
        })
        it("Update medicine: 200", async () => {
            const { body, statusCode } = await supertest(app).put("/medicine/1")
                .send({ "name": "Anarexix" })
            expect(statusCode).toBe(200)
            expect(body.name).toBe("Anarexix")
        })
    })

    describe("Delete medicine route", () => {
        it("Delete non-existing medicine : 404", async () => {
            const { statusCode } = await supertest(app).delete("/medicine/205")
            expect(statusCode).toBe(404)
        })
        it("Delete deleted medicine : 404", async () => {
            const { statusCode } = await supertest(app).delete("/medicine/100")
            expect(statusCode).toBe(404)
        })
        it("Delete medicine: 200", async () => {
            const { statusCode, body } = await supertest(app).delete("/medicine/2")
            expect(statusCode).toBe(200)
            expect(body.deleted_at).not.toBeNull()
        })
    })

    describe("Restore medicine route", () => {
        it("Restore non-existing medicine : 404", async () => {
            const { statusCode } = await supertest(app).post("/medicine/205/restore")
            expect(statusCode).toBe(404)
        })
        it("Restore non-deleted medicine : 404", async () => {
            const { statusCode } = await supertest(app).post("/medicine/1/restore")
            expect(statusCode).toBe(404)
        })
        it("Restore medicine: 200", async () => {
            const { statusCode, body } = await supertest(app).post("/medicine/100/restore")
            expect(statusCode).toBe(200)
            expect(body.deleted_at).toBeNull()

        })
    })
})

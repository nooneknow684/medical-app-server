import { z } from "zod";
import { paginationQuerySchema, preprocessString } from "../modules.schema";
import { medicineUnitExits } from "../modules.validation";

export const medicineUnitDetailParamScheme = z.object({
    unit: z.preprocess(preprocessString, z.number().refine(medicineUnitExits.validator(), medicineUnitExits.message))
})

export const medicineUnitDeletedDetailParamScheme = z.object({
    unit: z.preprocess(preprocessString, z.number().refine(medicineUnitExits.validator(true), medicineUnitExits.message))
})

export const getAlllMedicineUnitQuerySchema = paginationQuerySchema.extend({
    sort: z.enum(['name', '-name']).default('name'),
    name: z.string().optional(),
    deleted: z.preprocess((val) => val != undefined, z.boolean().default(false))
})

export const createMedicineUnitBodySchema = z.object({
    name: z.string().min(1),
    icon: z.string().min(1).optional(),
    color: z.string().min(1).optional()
})

export const updateMedicineUnitBodySchema = z.object({
    name: z.string().min(1).optional(),
    icon: z.string().min(1).optional(),
    color: z.string().min(1).optional()
})

export type GetAlllMedicineUnitQueryType = z.infer<typeof getAlllMedicineUnitQuerySchema>
export type MedicineUnitDetailParamScheme = z.infer<typeof medicineUnitDetailParamScheme>
export type CreateMedicineUnitBodyType = z.infer<typeof createMedicineUnitBodySchema>
export type UpdateMedicineUnitBodyType = z.infer<typeof updateMedicineUnitBodySchema>

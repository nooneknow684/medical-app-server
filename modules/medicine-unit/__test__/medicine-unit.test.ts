import supertest from "supertest"
import createApp from "../../../utils/app"
import { clearDatabase } from "../../../__test__/before-test"
import { medicineUnitFactory } from "../../../__test__/factory"

const app = createApp()

describe("Test Medicine Unit Routes", () => {
    beforeAll(async () => {
        await clearDatabase()
        await medicineUnitFactory([
            { id: 1, name: "Pil" },
            { id: 2, name: "Botol" },
            { id: 3, name: "Kapsul" },
            { id: 4, name: "Kaplet" },
            { id: 5, name: "Bungkus" },
            { id: 6, name: "Kehapus", deleted_at: new Date() }
        ])

    })

    describe("Get All Medicine unit Route", () => {
        it("Get all medicine unit", async () => {
            const { body, statusCode } = await supertest(app).get("/unit")

            expect(statusCode).toBe(200)

            expect(body.count).toBe(5)
            expect(body.data).toHaveLength(5)

            expect(body.data.map((e: any) => e.id)).toMatchObject([2, 5, 4, 3, 1])
        })

        it("Get all medicine unit dec name", async () => {
            const { body, statusCode } = await supertest(app)
                .get("/unit")
                .query({ 'sort': '-name' })

            expect(statusCode).toBe(200)

            expect(body.count).toBe(5)
            expect(body.data).toHaveLength(5)

            expect(body.data.map((e: any) => e.id)).toMatchObject([1, 3, 4, 5, 2])
        })

        it("Get all medicine unit that start with Ka", async () => {

            const { body, statusCode } = await supertest(app)
                .get("/unit")
                .query({ name: "ka" })

            expect(statusCode).toBe(200)
            expect(body.count).toBe(2)

            expect(body.data).toHaveLength(2)
        })
    })

    describe("Get specific medicine unit Route", () => {
        it("Get existing medicine unit", async () => {
            const { body, statusCode } = await supertest(app).get("/unit/1")
            expect(statusCode).toBe(200)
            expect(body.id).toBe(1)
        })

        it("Get non-existing medicine unit", async () => {
            const { body, statusCode } = await supertest(app).get("/unit/7")
            expect(statusCode).toBe(404)
            expect(body.error.code).toBe(404)
        })

        it("Get deleted medicine unit", async () => {
            const { body, statusCode } = await supertest(app).get("/unit/6")
            expect(statusCode).toBe(404)
            expect(body.error.code).toBe(404)
        })
    })

    describe("Create medicine Route", () => {
        it("Create medicine unit without name", async () => {
            const { statusCode } = await supertest(app)
                .post("/unit")
            expect(statusCode).toBe(400)
        })
        it("Create medicine unit", async () => {
            const { statusCode, body } = await supertest(app)
                .post("/unit")
                .send({ name: "Tablet" })

            expect(statusCode).toBe(201)
            expect(body.name).toBe("Tablet")
        })
    })
    describe("Update medicine unit Route", () => {
        it("Update medicine unit without name", async () => {
            const { statusCode, body } = await supertest(app)
                .put("/unit/1")
            expect(statusCode).toBe(200)
            expect(body.name).toBe('Pil')
        })

        it("Update not Existing medicine unit", async () => {
            const { statusCode } = await supertest(app)
                .put("/unit/9")
                .send({ name: "Super Tablet" })

            expect(statusCode).toBe(404)
        })

        it("Update deleted medicine unit", async () => {
            const { statusCode } = await supertest(app)
                .put("/unit/6")
                .send({ name: "Super Tablet" })

            expect(statusCode).toBe(404)
        })

        it("Update medicine unit", async () => {
            const { statusCode, body } = await supertest(app)
                .put("/unit/1")
                .send({ name: "Super Tablet" })

            expect(statusCode).toBe(200)
            expect(body.name).toBe("Super Tablet")
        })
    })

    describe("Delete Medicine unit route", () => {
        it("Delete non exisiting medicine unit", async () => {
            const { statusCode } = await supertest(app).delete('/unit/9')
            expect(statusCode).toBe(404)
        })
        it("Delete deleted medicine unit", async () => {
            const { statusCode } = await supertest(app).delete('/unit/6')
            expect(statusCode).toBe(404)
        })
        it("Delete deleted medicine unit", async () => {
            const { statusCode } = await supertest(app).delete('/unit/5')
            expect(statusCode).toBe(200)
        })
    })

    describe("Restore Medicine unit route", () => {
        it("Restore non existing medicine unit", async () => {
            const { statusCode } = await supertest(app).post('/unit/9/restore')
            expect(statusCode).toBe(404)

        })
        it("Restore non deleted medicine unit", async () => {
            const { statusCode } = await supertest(app).post('/unit/4/restore')
            expect(statusCode).toBe(404)
        })
        it("Restore medicine unit", async () => {
            const { statusCode } = await supertest(app).post('/unit/6/restore')
            expect(statusCode).toBe(200)
        })
    })
})

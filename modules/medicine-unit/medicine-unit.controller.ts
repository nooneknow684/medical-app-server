import { Response } from 'express'

import { paginateResponse, TypedRequest } from '../../utils/helper'

import { CreateMedicineUnitBodyType, MedicineUnitDetailParamScheme, GetAlllMedicineUnitQueryType, UpdateMedicineUnitBodyType } from './medicine-unit.schema'
import { createMedicineUnit, deleteMedicineUnit, getAllMedicineUnit, getMedicineUnit, restoreMedicineUnit, updateMedicineUnit } from './medicine-unit.service'

export async function getAllMedicineUnitHandler(req: TypedRequest<any, any, GetAlllMedicineUnitQueryType>, res: Response) {
    const [count, medicineUnits] = await getAllMedicineUnit(req.query)
    return res.status(200).json(paginateResponse(req.query, count, medicineUnits))
}

export async function getMedicineUnitHandler(req: TypedRequest<any, { unit: number }>, res: Response) {
    const medicine = await getMedicineUnit(req.params)
    return res.status(200).json(medicine)
}

export async function createMedicineUnitHandler(req: TypedRequest<CreateMedicineUnitBodyType>, res: Response) {
    const createdMedicine = await createMedicineUnit(req.body)
    return res.status(201).json(createdMedicine)
}

export async function updateMedicineUnitHandler(req: TypedRequest<UpdateMedicineUnitBodyType, MedicineUnitDetailParamScheme>, res: Response) {
    const updatedMedicine = await updateMedicineUnit(req.params, req.body)
    return res.status(200).json(updatedMedicine)
}

export async function deleteMedicineUnitHandler(req: TypedRequest<any, MedicineUnitDetailParamScheme>, res: Response) {
    const deletedMedicine = await deleteMedicineUnit(req.params)
    return res.status(200).json(deletedMedicine)
}

export async function restoreMedicineUnitHandler(req: TypedRequest<any, MedicineUnitDetailParamScheme>, res: Response) {
    const restoredMedicine = await restoreMedicineUnit(req.params)
    return res.status(200).json(restoredMedicine)
}

// async function _test() {
//     return await prisma.medicine.findMany({
//         select: {
//             id: true,
//             name: true,
//             qty_detail: {
//                 select: {
//                     id: true,
//                     qty: true,
//                     unit: {
//                         select: {
//                             name: true,
//                             id: true,
//                         }
//                     }
//                 }
//             }
//         },
//         orderBy: {
//             name: 'asc'
//         }
//     })
// }

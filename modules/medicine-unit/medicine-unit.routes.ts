import { Router } from 'express'
import { validateBody, validateParam, validateQuery } from '../../utils/helper'
import { createMedicineUnitHandler, deleteMedicineUnitHandler, getAllMedicineUnitHandler, getMedicineUnitHandler, restoreMedicineUnitHandler, updateMedicineUnitHandler } from './medicine-unit.controller'
import { createMedicineUnitBodySchema, medicineUnitDetailParamScheme, getAlllMedicineUnitQuerySchema, updateMedicineUnitBodySchema, medicineUnitDeletedDetailParamScheme } from './medicine-unit.schema'

const medicineUnitRoute = Router()

medicineUnitRoute.route('/')
    .get([validateQuery(getAlllMedicineUnitQuerySchema)], getAllMedicineUnitHandler)
    .post([validateBody(createMedicineUnitBodySchema)], createMedicineUnitHandler)

medicineUnitRoute.route('/:unit').all([validateParam(medicineUnitDetailParamScheme)])
    .get(getMedicineUnitHandler)
    .put([validateBody(updateMedicineUnitBodySchema)], updateMedicineUnitHandler)
    .delete(deleteMedicineUnitHandler)

medicineUnitRoute.post('/:unit/restore', [validateParam(medicineUnitDeletedDetailParamScheme)], restoreMedicineUnitHandler)

export default medicineUnitRoute

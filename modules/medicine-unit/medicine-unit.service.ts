import { Prisma } from "@prisma/client"
import prisma from "../../utils/prisma"
import { CreateMedicineUnitBodyType, MedicineUnitDetailParamScheme, GetAlllMedicineUnitQueryType, UpdateMedicineUnitBodyType } from "./medicine-unit.schema"

const defaultSelect: Prisma.MedicineUnitSelect = {
    id: true,
    name: true,
    color: true,
    icon: true,
    deleted_at: true,
    created_at: true,
    updated_at: true
}

function findFirstMedicineUnit({ unit }: MedicineUnitDetailParamScheme) {
    return prisma.medicineUnit.findFirstOrThrow({ where: { id: unit, deleted_at: null }, select: defaultSelect })
}

export async function getAllMedicineUnit(query: GetAlllMedicineUnitQueryType) {
    const filterOption: Prisma.MedicineUnitWhereInput = {
        deleted_at: query.deleted ? undefined : null,
        name: {
            startsWith: query.name
        }
    }

    const orderBy: Prisma.MedicineUnitOrderByWithRelationInput = {
        name: query.sort == "name" ? 'asc' : query.sort == "-name" ? "desc" : undefined
    }

    return await prisma.$transaction([
        prisma.medicineUnit.count({ where: filterOption, }),
        prisma.medicineUnit.findMany({ orderBy, select: defaultSelect, where: filterOption, take: query.per_page, skip: (query.page - 1) * query.per_page })
    ])
}

export async function getMedicineUnit({ unit }: MedicineUnitDetailParamScheme) {
    return await findFirstMedicineUnit({ unit })
}

export async function createMedicineUnit(data: CreateMedicineUnitBodyType) {
    return await prisma.medicineUnit.create({
        data: data, select: defaultSelect
    })
}

export async function updateMedicineUnit({ unit }: MedicineUnitDetailParamScheme, data: UpdateMedicineUnitBodyType) {
    const updatedMedicineUnit = await prisma.medicineUnit.update({
        where: {
            id: unit
        },
        data,
        select: defaultSelect
    })
    return updatedMedicineUnit
}

export async function deleteMedicineUnit({ unit }: MedicineUnitDetailParamScheme) {
    const updatedMedicineUnit = await prisma.medicineUnit.update({
        where: {
            id: unit
        },
        data: {
            deleted_at: new Date()
        },
        select: defaultSelect
    })
    return updatedMedicineUnit
}

export async function restoreMedicineUnit({ unit }: MedicineUnitDetailParamScheme) {
    const restoredMedicineUnit = await prisma.medicineUnit.update({ where: { id: unit }, data: { deleted_at: null }, select: defaultSelect })
    return restoredMedicineUnit

}

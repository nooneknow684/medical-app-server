import { z } from "zod";
import { paginationQuerySchema, preprocessString } from "../modules.schema";
import { patientExists } from "../modules.validation";
import { uniqueNoKtp } from "./patient.validation";

export const patientDetailParamSchema = z.object({
    patient: z.preprocess(preprocessString, z.number().refine(patientExists.validator(), patientExists.message))
})

export const patientDetailDeletedParamSchema = z.object({
    patient: z.preprocess(preprocessString, z.number().refine(patientExists.validator(true), patientExists.message))
})

export const getAllPatientsQueryScheme = paginationQuerySchema.extend({
    name: z.string().optional(),
    no_ktp: z.string().optional(),
    sex: z.enum(['male', 'female']).optional(),
    sort: z.enum(['name', '-name', 'no_ktp', '-no_ktp']).default('name')
})


export const createPatientBodySchema = z.object({
    name: z.string().min(1),
    no_ktp: z.string().refine(uniqueNoKtp.validator, uniqueNoKtp.message),
    birthdate: z.preprocess((val) => new Date(String(val)), z.date().max(new Date()),),
    sex: z.enum(['male', 'female']),
    sickness: z.array(z.string()).optional()
})

export const updatePatientBodySchema = z.object({
    name: z.string().min(0).optional(),
    no_ktp: z.string().length(20).optional(),
    birthdate: z.date().max(new Date()).optional(),
    gender: z.enum(['male', 'female']).optional(),
    sickness: z.array(z.string()).optional()
})

export type PatientDetailParamType = z.infer<typeof patientDetailParamSchema>
export type GetAllPatientsQueryType = z.infer<typeof getAllPatientsQueryScheme>
export type CreatePatientBodyType = z.infer<typeof createPatientBodySchema>
export type UpdatePatientBodyType = z.infer<typeof updatePatientBodySchema>

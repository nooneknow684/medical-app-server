import { Prisma } from "@prisma/client";
import { sortChekcer } from "../../utils/helper";
import prisma from "../../utils/prisma";
import { CreatePatientBodyType, GetAllPatientsQueryType, PatientDetailParamType, UpdatePatientBodyType } from "./patient.schema";

const defaultSelect: Prisma.PatientSelect = {
    id: true,
    name: true,
    birthdate: true,
    sex: true,
    sickness: true,
    created_at: true,
    deleted_at: true,
    updated_at: true,
    visits: {
        select: {
            id: true,
            visit_date: true,
            description: true
        },
        where: {
            deleted_at: null
        }
    }
}
export async function getAllPatients(query: GetAllPatientsQueryType) {
    const filter: Prisma.PatientWhereInput = {
        deleted_at: null,
        name: {
            startsWith: query.name
        },
        sex: query.sex,
        no_ktp: query.no_ktp
    }

    const sort: Prisma.PatientOrderByWithRelationInput = {
        name: sortChekcer('name', query.sort),
        no_ktp: sortChekcer('no_ktp', query.sort),
    }


    return prisma.$transaction([
        prisma.patient.count({ where: filter }),
        prisma.patient.findMany({ where: filter, orderBy: sort, take: query.per_page, skip: (query.page - 1) * query.per_page, select: { ...defaultSelect, sickness: false, visits: false } })
    ])
}

export async function getPatient(param: PatientDetailParamType) {
    return await prisma.patient.findFirst({ where: { deleted_at: null, id: param.patient }, select: defaultSelect })
}

export async function createPatient(data: CreatePatientBodyType) {
    const { sickness, ...patientData } = data
    return await prisma.patient.create({
        data: {
            ...patientData,
            sickness: sickness ?? []
        },
        select: defaultSelect
    })
}

export async function updatePatient(param: PatientDetailParamType, data: UpdatePatientBodyType) {
    return prisma.patient.update({
        where: {
            id: param.patient
        },
        data: data,
        select: defaultSelect
    })
}

export async function deletePatient(param: PatientDetailParamType) {
    return await prisma.patient.update({
        where: {
            id: param.patient
        },
        data: {
            deleted_at: new Date()
        }, select: defaultSelect
    })
}

export async function restorePatient(param: PatientDetailParamType) {
    return await prisma.patient.update({
        where: {
            id: param.patient
        },
        data: {
            deleted_at: null
        }, select: defaultSelect
    })
}

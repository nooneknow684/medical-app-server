import prisma from "../../../utils/prisma"
import supertest from "supertest"
import createApp from "../../../utils/app"
import { clearDatabase } from "../../../__test__/before-test"
import { mockPatient } from "../../../__test__/factory"

const app = createApp()

describe("Test Patient Route", () => {
    beforeAll(async () => {
        await clearDatabase()
        await mockPatient()
    })

    describe("Get all patients test", () => {

        it("Get all patient", async () => {
            const { body, statusCode } = await supertest(app).get("/patient")
            expect(statusCode).toBe(200)
            expect(body.count).toBe(87)
            expect(body.data).toHaveLength(10)
        })
        it("Get all patient ordered by name desc", async () => {
            const { body, statusCode } = await supertest(app).get("/patient").query({ sort: '-name' })
            expect(statusCode).toBe(200)
            expect(body.count).toBe(87)
            expect(body.data).toHaveLength(10)
        })
        it("Get all patient ordered by paginated as 15", async () => {
            const { body, statusCode } = await supertest(app).get("/patient").query({ per_page: 15, page: 2 })
            expect(statusCode).toBe(200)
            expect(body.count).toBe(87)
            expect(body.data).toHaveLength(15)
        })
        it("Get all patient ordered by paginated as 15 at last page", async () => {
            const { body, statusCode } = await supertest(app).get("/patient").query({ per_page: 15, page: 6 })
            expect(statusCode).toBe(200)
            expect(body.count).toBe(87)
            expect(body.data).toHaveLength(12)
        })
        it("Get all patient ordered by paginated as 15 exceeding page", async () => {
            const { body, statusCode } = await supertest(app).get("/patient").query({ per_page: 15, page: 8 })
            expect(statusCode).toBe(200)
            expect(body.count).toBe(87)
            expect(body.data).toHaveLength(0)
        })
        it("Get all patient start with Pa", async () => {
            const { body, statusCode } = await supertest(app).get("/patient").query({ name: 'pa', })
            const count = await prisma.patient.count({ where: { name: { startsWith: 'pa' }, deleted_at: null } })
            expect(statusCode).toBe(200)
            expect(body.count).toBe(count)
            expect(body.data).toHaveLength(Math.min(count, 10))
        })
        it("Get all patient start with sex male", async () => {
            const { body, statusCode } = await supertest(app).get("/patient").query({ sex: 'male' })
            const count = await prisma.patient.count({ where: { sex: 'male', deleted_at: null } })
            expect(statusCode).toBe(200)
            expect(body.count).toBe(count)
            expect(body.data).toHaveLength(Math.min(count, 10))
        })
        it("Get all patient start with no_ktp 123", async () => {
            const { body, statusCode } = await supertest(app).get("/patient").query({ no_ktp: '123' })
            const count = await prisma.patient.count({ where: { no_ktp: { startsWith: "123" }, deleted_at: null } })
            expect(statusCode).toBe(200)
            expect(body.count).toBe(count)
            expect(body.data).toHaveLength(Math.min(count, 10))
        })
    })

    describe("Create patient test", () => {
        it("Create patient without name", async () => {
            const payload = {
                sex: "male",
                birthdate: '2008/02/02',
                no_ktp: '1234',
                sickness: []

            }
            const { statusCode } = await supertest(app).post("/patient").send(payload)
            expect(statusCode).toBe(400)
        })

        it("Create patient without sex", async () => {
            const payload = {
                name: "Test name",
                birthdate: '2008/02/02',
                no_ktp: '1234',
                sickness: []

            }
            const { statusCode } = await supertest(app).post("/patient").send(payload)
            expect(statusCode).toBe(400)
        })
        it("Create patient random sex value", async () => {
            const payload = {
                name: "Test name",
                sex: "malesamat",
                birthdate: '2008/02/02',
                no_ktp: '1234',
                sickness: []

            }
            const { statusCode } = await supertest(app).post("/patient").send(payload)
            expect(statusCode).toBe(400)
        })
        it("Create patient without birthdate", async () => {
            const payload = {
                name: "Test name",
                sex: "male",
                no_ktp: '1234',
                sickness: []

            }
            const { statusCode } = await supertest(app).post("/patient").send(payload)
            expect(statusCode).toBe(400)
        })
        it("Create patient with birthdate on the future", async () => {
            const payload = {
                name: "Test name",
                sex: "male",
                birthdate: '2023/02/02',
                no_ktp: '1234',
                sickness: []

            }
            const { statusCode } = await supertest(app).post("/patient").send(payload)
            expect(statusCode).toBe(400)
        })
        it("Create patient without ktp", async () => {
            const payload = {
                name: "Test name",
                sex: "male",
                birthdate: '2008/02/02',
                sickness: []

            }
            const { statusCode } = await supertest(app).post("/patient").send(payload)
            expect(statusCode).toBe(400)
        })
        it("Create patient with non-uniqe ktp", async () => {
            const payload = {
                name: "Test name",
                sex: "male",
                birthdate: '2008/02/02',
                no_ktp: '3695729902',
                sickness: []

            }
            const { statusCode } = await supertest(app).post("/patient").send(payload)
            expect(statusCode).toBe(400)
        })
        it("Create patient", async () => {
            const payload = {
                name: "Test name",
                sex: "male",
                birthdate: '2008/02/02',
                no_ktp: '1234',
                sickness: []

            }
            const { statusCode } = await supertest(app).post("/patient").send(payload)
            expect(statusCode).toBe(201)
        })
    })

    describe("Update patient route", () => {

    })
})

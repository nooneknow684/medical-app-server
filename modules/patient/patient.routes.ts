import { Router } from 'express'
import { validateBody, validateParam, validateQuery } from '../../utils/helper'
import { createPatientHandler, deletePatientHandler, getAllPatientsHandler, getPatientHandler, restorePatientHandler, updatePatientHandler } from './patient.controller'
import { createPatientBodySchema, getAllPatientsQueryScheme, patientDetailDeletedParamSchema, patientDetailParamSchema, updatePatientBodySchema } from './patient.schema'

const patientRoute = Router()

patientRoute.route('/')
    .get([validateQuery(getAllPatientsQueryScheme)], getAllPatientsHandler)
    .post([validateBody(createPatientBodySchema)], createPatientHandler)

patientRoute.route('/:patient').all([validateParam(patientDetailParamSchema)])
    .get([], getPatientHandler)
    .put([validateBody(updatePatientBodySchema)], updatePatientHandler)
    .delete([], deletePatientHandler)

patientRoute.post('/:medicine/restore', [validateParam(patientDetailDeletedParamSchema)], restorePatientHandler)

export default patientRoute

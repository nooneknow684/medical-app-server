import { Response } from 'express'
import { paginateResponse, TypedRequest } from "../../utils/helper";
import { CreatePatientBodyType, GetAllPatientsQueryType, PatientDetailParamType, updatePatientBodySchema, UpdatePatientBodyType } from "./patient.schema";
import { createPatient, deletePatient, getAllPatients, getPatient, restorePatient, updatePatient } from './patient.service';

export async function getAllPatientsHandler(req: TypedRequest<any, any, GetAllPatientsQueryType>, res: Response) {
    const [count, patients] = await getAllPatients(req.query)
    return res.status(200).json(paginateResponse(req.query, count, patients))

}

export async function getPatientHandler(req: TypedRequest<any, PatientDetailParamType>, res: Response) {
    const patient = await getPatient(req.params)
    return res.status(200).json(patient)

}

export async function createPatientHandler(req: TypedRequest<CreatePatientBodyType>, res: Response) {
    const createdPatient = await createPatient(req.body)
    return res.status(201).json(createdPatient)

}

export async function updatePatientHandler(req: TypedRequest<UpdatePatientBodyType, PatientDetailParamType>, res: Response) {
    const updatedPatient = await updatePatient(req.params, req.body)
    return res.status(200).json(updatedPatient)

}

export async function deletePatientHandler(req: TypedRequest<any, PatientDetailParamType>, res: Response) {
    const deletedPatient = await deletePatient(req.params)
    return res.status(200).json(deletedPatient)
}

export async function restorePatientHandler(req: TypedRequest<any, PatientDetailParamType>, res: Response) {
    const restoredPatient = await restorePatient(req.params)
    return res.status(200).json(restoredPatient)
}

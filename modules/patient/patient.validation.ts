import prisma from "../../utils/prisma";

export const uniqueNoKtp = {
    validator: async (data: string) => (await prisma.patient.count({ where: { no_ktp: data } })) === 0,
    message: (result: string) => ({ message: `no_ktp ${result} is already exists` })
}

import { Router } from 'express'
import { validateBody, validateParam } from '../../utils/helper'
import { medicineDetailParamSchema } from '../medicine/medicine.schema'
import { postMedicineDetailHandler, putMedicineDetailHandler } from './medicine-detail.controller'
import { postMedicineDetailBodySchema, putMedicineDetailBodySchema } from './medicine-detail.schema'

const medicineDetailRoutes = Router()

medicineDetailRoutes.route("/:medicine/unit").all([validateParam(medicineDetailParamSchema)])
    .post([validateBody(postMedicineDetailBodySchema)], postMedicineDetailHandler)
    .put([validateBody(putMedicineDetailBodySchema)], putMedicineDetailHandler)

export default medicineDetailRoutes

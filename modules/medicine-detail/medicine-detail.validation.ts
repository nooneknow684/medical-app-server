import { RefinementCtx, z } from "zod";
import prisma from "../../utils/prisma";
import { ValidateMedicineDetailQtyType } from "./medicine-detail.schema";

export const medicineWithQty = async ({ param, body }: ValidateMedicineDetailQtyType, ctx: RefinementCtx) => {
    if (body.from_unit_id === body.to_unit_id) {
        ctx.addIssue({
            path: ["body", "to_unit_id"],
            code: z.ZodIssueCode.custom,
            message: "to_unit_id cannot equal to from_unit_id",
            fatal: true
        })
        return z.NEVER
    }
    const medicineDetail = await prisma.medicineDetail.findFirst({ where: { medicine_id: param.medicine, unit_id: body.from_unit_id }, select: { qty: true } })
    if (medicineDetail) {
        if (medicineDetail.qty < body.from_unit_qty)
            ctx.addIssue({
                path: ["body", "from_unit_qty"],
                code: z.ZodIssueCode.too_big,
                maximum: medicineDetail.qty,
                inclusive: true,
                type: "number",
                message: `medicine has less then ${body.from_unit_qty} of unit with id ${body.from_unit_id}`
            })
    } else {
        ctx.addIssue({
            path: ["body", "from_unit_id"],
            code: z.ZodIssueCode.custom,
            message: `medicine doesn't has unit with id ${body.from_unit_id}`
        })
    }
}

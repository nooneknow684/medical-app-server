import { z } from "zod";
import { medicineUnitExits } from "../modules.validation";


export const postMedicineDetailBodySchema = z.object({
    unit_id: z.number().refine(medicineUnitExits.validator(), medicineUnitExits.message),
    qty: z.number().positive()
})

export const putMedicineDetailBodySchema = z.object({
    from_unit_id: z.number(),
    to_unit_id: z.number().positive().refine(medicineUnitExits.validator(), medicineUnitExits.message),
    from_unit_qty: z.number().positive(),
    multiplier: z.number().positive()
})

export const validateMedicineDetailQtySchema = z.object({
    body: z.object({
        from_unit_id: z.number(),
        from_unit_qty: z.number().positive(),
        to_unit_id: z.number()
    }),
    param: z.object({
        medicine: z.number()
    })
})

export type ValidateMedicineDetailQtyType = z.infer<typeof validateMedicineDetailQtySchema>
export type PostMedicineDetailBodyType = z.infer<typeof postMedicineDetailBodySchema>
export type PutMedicineDetailBodyType = z.infer<typeof putMedicineDetailBodySchema>


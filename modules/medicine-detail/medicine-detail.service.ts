import prisma from "../../utils/prisma";
import { MedicineDetailParamType } from "../medicine/medicine.schema";
import { PostMedicineDetailBodyType, PutMedicineDetailBodyType, validateMedicineDetailQtySchema } from "./medicine-detail.schema";
import { medicineWithQty } from "./medicine-detail.validation";

export async function createOrUpdateMedicineDetail(param: MedicineDetailParamType, data: PostMedicineDetailBodyType) {
    await prisma.medicineDetail.upsert({
        where: {
            unit_id_medicine_id: {
                medicine_id: param.medicine,
                unit_id: data.unit_id
            }
        },
        update: {
            qty: {
                increment: data.qty
            }
        },
        create: {
            medicine_id: param.medicine,
            unit_id: data.unit_id,
            qty: data.qty
        }
    })

}

export async function convertMedicineUnit(param: MedicineDetailParamType, data: PutMedicineDetailBodyType) {
    await validateMedicineDetailQtySchema.superRefine(medicineWithQty).parseAsync({ param, body: data })

    const reduceFromUnit = prisma.medicineDetail.update({
        where: {
            unit_id_medicine_id: {
                unit_id: data.from_unit_id,
                medicine_id: param.medicine
            },
        }, data: {
            qty: {
                decrement: data.from_unit_qty
            }
        }
    })

    const increaseTargetUnit = prisma.medicineDetail.upsert({
        where: {
            unit_id_medicine_id: {
                unit_id: data.to_unit_id,
                medicine_id: param.medicine
            }
        }, update: {
            qty: {
                increment: data.from_unit_qty * data.multiplier
            }
        }, create: {
            qty: data.from_unit_qty * data.multiplier,
            unit_id: data.to_unit_id,
            medicine_id: param.medicine
        }
    })

    await prisma.$transaction([reduceFromUnit, increaseTargetUnit])
}

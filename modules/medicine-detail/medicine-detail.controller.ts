import { Response } from 'express'
import { TypedRequest } from "../../utils/helper";
import { MedicineDetailParamType } from '../medicine/medicine.schema';
import { PostMedicineDetailBodyType, PutMedicineDetailBodyType } from "./medicine-detail.schema";
import { convertMedicineUnit, createOrUpdateMedicineDetail } from './medicine-detail.service';

export async function postMedicineDetailHandler(req: TypedRequest<PostMedicineDetailBodyType, MedicineDetailParamType>, res: Response) {
    await createOrUpdateMedicineDetail(req.params, req.body)
    return res.status(204).json()
}

export async function putMedicineDetailHandler(req: TypedRequest<PutMedicineDetailBodyType, MedicineDetailParamType>, res: Response) {
    await convertMedicineUnit(req.params, req.body)
    return res.status(204).json()
}

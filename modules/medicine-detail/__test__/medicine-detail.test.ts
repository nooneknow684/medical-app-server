import supertest from "supertest"
import createApp from "../../../utils/app"
import prisma from "../../../utils/prisma"
import { clearDatabase } from "../../../__test__/before-test"
import { medicineFactory, medicineUnitFactory } from "../../../__test__/factory"

const app = createApp()

describe("Test medicine detail routes", () => {
    beforeAll(async () => {
        await clearDatabase()
        await medicineUnitFactory([
            { id: 1, name: "Pak" },
            { id: 2, name: "Pil" },
            { id: 3, name: "Botol", deleted_at: new Date() }
        ])
        await medicineFactory([
            { id: 1, name: "Parasetamol" },
            { id: 2, name: "Konidin" },
            { id: 3, name: "Pop", deleted_at: new Date() }
        ])
    })
    describe("Add medicine detail test", () => {
        it("Add to non-existing medicine", async () => {
            const { statusCode } = await supertest(app).post("/medicine/4/unit")
                .send({ unit_id: 1, qty: 20 })

            expect(statusCode).toBe(404)
        })
        it("Add to deleted medicine", async () => {
            const { statusCode } = await supertest(app).post("/medicine/3/unit")
                .send({ unit_id: 1, qty: 20 })

            expect(statusCode).toBe(404)
        })

        it("Add non existing unit", async () => {
            const { statusCode } = await supertest(app).post("/medicine/1/unit")
                .send({ unit_id: 5, qty: 20 })

            expect(statusCode).toBe(400)
        })

        it("Add deleted unit", async () => {
            const { statusCode } = await supertest(app).post("/medicine/1/unit")
                .send({ unit_id: 3, qty: 20 })

            expect(statusCode).toBe(400)
        })

        it("Add to unit without unit id", async () => {
            const { statusCode } = await supertest(app).post("/medicine/1/unit")
                .send({ qty: 20 })

            expect(statusCode).toBe(400)
        })
        it("Add to unit without qty", async () => {
            const { statusCode } = await supertest(app).post("/medicine/1/unit")
                .send({ unit_id: 1 })

            expect(statusCode).toBe(400)
        })
        it("Add to unit", async () => {
            const { statusCode } = await supertest(app).post("/medicine/1/unit")
                .send({ unit_id: 1, qty: 20 })

            expect(statusCode).toBe(204)
        })

        it("Add to unit again", async () => {
            const { statusCode } = await supertest(app).post("/medicine/1/unit")
                .send({ unit_id: 1, qty: 60 })

            const result = await prisma.medicineDetail.count({ where: { medicine_id: 1, unit_id: 1, qty: 80 } })

            expect(statusCode).toBe(204)
            expect(result).toBe(1)
        })

    })
    describe("Convert medicine detail test", () => {
        it("Convert non-existing medicine", async () => {
            const { statusCode } = await supertest(app).put("/medicine/9/unit")
                .send({ from_unit_id: 1, to_unit_id: 2, from_unit_qty: 10, multiplier: 5 })
            expect(statusCode).toBe(404)
        })

        it("Convert deleted medicine", async () => {
            const { statusCode } = await supertest(app).put("/medicine/9/unit")
                .send({ from_unit_id: 1, to_unit_id: 2, from_unit_qty: 10, multiplier: 5 })
            expect(statusCode).toBe(404)
        })

        it("Convert from non-existing unit on medicine", async () => {
            const { statusCode } = await supertest(app).put("/medicine/1/unit")
                .send({ from_unit_id: 9, to_unit_id: 2, from_unit_qty: 10, multiplier: 5 })
            expect(statusCode).toBe(400)
        })

        it("Convert to non-existing unit", async () => {
            const { statusCode } = await supertest(app).put("/medicine/1/unit")
                .send({ from_unit_id: 1, to_unit_id: 9, from_unit_qty: 10, multiplier: 5 })
            expect(statusCode).toBe(400)
        })

        it("Convert to same unit", async () => {
            const { statusCode } = await supertest(app).put("/medicine/1/unit")
                .send({ from_unit_id: 1, to_unit_id: 1, from_unit_qty: 10, multiplier: 5 })
            expect(statusCode).toBe(400)
        })

        it("Convert more than available", async () => {
            const { statusCode } = await supertest(app).put("/medicine/1/unit")
                .send({ from_unit_id: 1, to_unit_id: 2, from_unit_qty: 100, multiplier: 5 })
            expect(statusCode).toBe(400)
        })
        it("Convert without from_unit_id", async () => {
            const { statusCode } = await supertest(app).put("/medicine/1/unit")
                .send({ to_unit_id: 2, from_unit_qty: 10, multiplier: 5 })
            expect(statusCode).toBe(400)
        })

        it("Convert without to_unit_id", async () => {
            const { statusCode } = await supertest(app).put("/medicine/1/unit")
                .send({ from_unit_id: 2, from_unit_qty: 10, multiplier: 5 })
            expect(statusCode).toBe(400)
        })
        it("Convert without from_unit_qty", async () => {
            const { statusCode } = await supertest(app).put("/medicine/1/unit")
                .send({ from_unit_id: 1, to_unit_id: 2, multiplier: 5 })
            expect(statusCode).toBe(400)
        })
        it("Convert without multiplier", async () => {
            const { statusCode } = await supertest(app).put("/medicine/1/unit")
                .send({ from_unit_id: 1, to_unit_id: 2, from_unit_qty: 5 })
            expect(statusCode).toBe(400)
        })
        it("Convert medicine", async () => {
            const { statusCode } = await supertest(app).put("/medicine/1/unit")
                .send({ from_unit_id: 1, to_unit_id: 2, from_unit_qty: 10, multiplier: 5 })
            const [detail1, detail2] = await prisma.$transaction([
                prisma.medicineDetail.count({ where: { medicine_id: 1, unit_id: 1, qty: 70 } }),
                prisma.medicineDetail.count({ where: { medicine_id: 1, unit_id: 2, qty: 50 } })
            ])
            expect(statusCode).toBe(204)
            expect(detail1).toBeTruthy()
            expect(detail2).toBeTruthy()
        })

        it("Convert medicine again", async () => {
            const { statusCode } = await supertest(app).put("/medicine/1/unit")
                .send({ from_unit_id: 1, to_unit_id: 2, from_unit_qty: 10, multiplier: 5 })
            const [detail1, detail2] = await prisma.$transaction([
                prisma.medicineDetail.count({ where: { medicine_id: 1, unit_id: 1, qty: 60 } }),
                prisma.medicineDetail.count({ where: { medicine_id: 1, unit_id: 2, qty: 100 } })
            ])
            expect(statusCode).toBe(204)
            expect(detail1).toBeTruthy()
            expect(detail2).toBeTruthy()
        })
    })
})

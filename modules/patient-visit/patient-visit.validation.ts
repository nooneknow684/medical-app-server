import { RefinementCtx, z } from "zod"
import prisma from "../../utils/prisma"

export const medicinesWithQty = async (data: { medicine_detail_id: number, qty: number }[], ctx: RefinementCtx) => {
    const medicines = await prisma.medicineDetail.findMany({ where: { id: { in: data.map(e => e.medicine_detail_id) } }, select: { id: true, qty: true } })
    const medicineMap = new Map(medicines.map(e => ([e.id, e.qty])))

    var validation_error = false

    data.forEach((e, index) => {
        const fromMap = medicineMap.get(e.medicine_detail_id)
        if (!fromMap) {
            ctx.addIssue({
                code: z.ZodIssueCode.custom,
                path: [index, 'medicine_detail_id'],
                message: `no medicine with id ${e.medicine_detail_id}`
            })
            validation_error = true
            return
        }

        if (fromMap <= e.qty) {
            ctx.addIssue({
                code: z.ZodIssueCode.custom,
                path: [index, 'qty'],
                message: `this medicine only has ${fromMap} unit`
            })
            validation_error = true
            return
        }
    })
    if (validation_error) return z.NEVER

    return medicines.map(({ id, ...other }) => ({ ...other, medicine_detail_id: id, current_price: 12 }))
}


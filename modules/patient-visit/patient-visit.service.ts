import { Prisma } from "@prisma/client";
import prisma from "../../utils/prisma";
import { CreatePatientVisitBodyType, GetAllPatientVisitsQueryType, PatientVisitDetailParamType } from "./patient-visit.scheme";

const allSelect: Prisma.PatientVisitSelect = {
    id: true,
    visit_date: true,
    next_visit_date: true,
    description: true,

    patient: {
        select: {
            id: true,
            name: true,
        }
    },
}

const detailSelect: Prisma.PatientVisitSelect = {
    id: true,
    visit_date: true,
    next_visit_date: true,
    description: true,
    total: true,

    symtoms: true,
    patient: {
        select: {
            id: true,
            name: true,
            no_ktp: true,
            sickness: true
        }
    },


    medicines: {
        select: {
            current_price: true,
            qty: true,
            medicine_detail: {
                select: {
                    id: true,
                    unit: {
                        select: {
                            id: true,
                            name: true
                        }
                    },
                    medicine: {
                        select: {
                            id: true,
                            name: true
                        }
                    }
                }
            }
        }
    },
    next_visit: {
        select: {
            id: true,
            visit_date: true
        },
    },
    previous_visit: {
        select: {
            id: true,
            visit_date: true
        },
    }
}

export async function getAllPatientVisit(query: GetAllPatientVisitsQueryType) {
    const filter: Prisma.PatientVisitWhereInput = {
        deleted_at: null,
        visit_date: {
            lte: query.visit_before,
            gte: query.visit_after
        },
        patient: {
            name: {
                startsWith: query.patient_name
            }
        }
    }
    return await prisma.$transaction([
        prisma.patientVisit.count({ where: filter }),
        prisma.patientVisit.findMany({ where: filter, select: allSelect, take: query.per_page, skip: (query.page - 1) * query.per_page })
    ])
}

export async function getPatientVisit(param: PatientVisitDetailParamType) {
    return await prisma.patientVisit.findFirstOrThrow({ select: detailSelect, where: { deleted_at: null, id: param.visit } })
}

export async function createPatientVisit(data: CreatePatientVisitBodyType) {
    const { medicines, ...patientVisitData } = data

    const createPatientVisitPromise = prisma.patientVisit.create({
        data: {
            ...patientVisitData,
            medicines: {
                createMany: {
                    data: medicines ?? []
                }
            },
        },
        select: detailSelect
    })

    const [createdPatient,] = await prisma.$transaction([
        createPatientVisitPromise,
        ...(medicines?.map(e => prisma.medicineDetail.update({ select: null, where: { id: e.medicine_detail_id }, data: { qty: { decrement: e.qty } } })) ?? [])
    ])

    return createdPatient
}

export async function deletePatientVisit(param: PatientVisitDetailParamType) {
    const medicines = await prisma.patientVisitMedicine.findMany({ where: { visit_id: param.visit }, select: { medicine_detail_id: true, qty: true } })
    const [, deletedPatientVisits] = await prisma.$transaction([
        ...medicines.map(({ medicine_detail_id, qty }) => prisma.medicineDetail.update({ where: { id: medicine_detail_id }, data: { qty: { increment: qty } } })),
        prisma.patientVisit.delete({
            where: {
                id: param.visit
            },
            select: detailSelect
        })]

    )
    return deletedPatientVisits
}

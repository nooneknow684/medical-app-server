import { z } from "zod";
import { paginationQuerySchema, preprocessDate, preprocessString } from "../modules.schema";
import { patientExists, patientVisitExists } from "../modules.validation";
import { medicinesWithQty } from "./patient-visit.validation";

export const patientVisitDetailParamScheme = z.object({
    visit: z.preprocess(preprocessString, z.number())
})

export const getAllPatientVisitsQuerySchema = paginationQuerySchema.extend({
    patient_name: z.string().min(1).optional(),
    visit_before: z.preprocess(preprocessDate, z.date()).optional(),
    visit_after: z.preprocess(preprocessDate, z.date()).optional(),

    sort: z.enum(['patient_name', '-patient_name', 'visit_date', '-visit_date']).default('visit_date')
})

export const createPatientVisitBodySchema = z.object({
    patient_id: z.number().refine(patientExists.validator(), patientExists.message),
    previous_visit_id: z.number().refine(patientVisitExists.validator(), patientVisitExists.message).optional(),
    visit_date: z.preprocess(preprocessDate, z.date()),
    next_visit_date: z.preprocess(preprocessDate, z.date()).optional(),
    description: z.string().min(1).optional(),
    total: z.number().positive().default(0),
    symtoms: z.array(z.string()).default([]),
    medicines: z.array(
        z.object({
            medicine_detail_id: z.number(),
            qty: z.number().positive()
        })
    ).transform(medicinesWithQty).optional()
})

export type PatientVisitDetailParamType = z.infer<typeof patientVisitDetailParamScheme>
export type GetAllPatientVisitsQueryType = z.infer<typeof getAllPatientVisitsQuerySchema>
export type CreatePatientVisitBodyType = z.infer<typeof createPatientVisitBodySchema>

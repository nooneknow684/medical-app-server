import prisma from "../../../utils/prisma";
import { clearDatabase } from "../../../__test__/before-test"
import { medicineUnitFactory, mockMedicine, mockPatient } from "../../../__test__/factory";

describe("Patient visit route test", () => {
    beforeAll(async () => {
        await clearDatabase();
        await mockPatient();
        await mockMedicine();

        await medicineUnitFactory([
            { id: 1, name: "Pil" },
            { id: 2, name: "Botol" },
            { id: 3, name: "Kapsul" },
            { id: 4, name: "Kaplet" },
            { id: 5, name: "Bungkus" },
            { id: 6, name: "Kehapus", deleted_at: new Date() }
        ])

        await prisma.medicineDetail.createMany({
            data: [{
                id: 1,
                qty: 50,
                unit_id: 1,
                medicine_id: 1
            }, {
                id: 2,
                qty: 40,
                unit_id: 1,
                medicine_id: 2
            }, {
                id: 3,
                qty: 33,
                unit_id: 1,
                medicine_id: 3
            }, {
                id: 4,
                qty: 84,
                unit_id: 1,
                medicine_id: 4
            }, {
                id: 5,
                qty: 19,
                unit_id: 1,
                medicine_id: 5
            },]
        })

    })
})

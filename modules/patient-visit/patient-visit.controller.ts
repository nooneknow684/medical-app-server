import { Response } from "express";
import { paginateResponse, TypedRequest } from "../../utils/helper";
import { CreatePatientVisitBodyType, GetAllPatientVisitsQueryType, PatientVisitDetailParamType } from "./patient-visit.scheme";
import { createPatientVisit, deletePatientVisit, getAllPatientVisit, getPatientVisit } from "./patient-visit.service";

export async function getAllPatientVisitsHandler(req: TypedRequest<any, any, GetAllPatientVisitsQueryType>, res: Response) {
    const [count, patientVisits] = await getAllPatientVisit(req.query)
    return res.status(200).json(paginateResponse(req.query, count, patientVisits))
}
export async function getPatientVisitHandler(req: TypedRequest<any, PatientVisitDetailParamType>, res: Response) {
    const patientVisit = await getPatientVisit(req.params)
    return res.status(200).json(patientVisit)
}
export async function createPatientVisitHandler(req: TypedRequest<CreatePatientVisitBodyType>, res: Response) {
    const createdPatientVisit = await createPatientVisit(req.body)
    return res.status(200).json(createdPatientVisit)
}
export async function deletePatientVisitHandler(req: TypedRequest<any, PatientVisitDetailParamType>, res: Response) {
    const deletedPatientVisit = await deletePatientVisit(req.params)
    return res.status(200).json(deletedPatientVisit)
}
export async function restorePatientVisitHandler(req: TypedRequest<any, PatientVisitDetailParamType>, res: Response) {
    return res.status(200).json()
}

import { Router } from 'express'
import { validateBody, validateParam, validateQuery } from '../../utils/helper'
import { createPatientVisitHandler, deletePatientVisitHandler, getAllPatientVisitsHandler, getPatientVisitHandler, restorePatientVisitHandler } from './patient-visit.controller'
import { createPatientVisitBodySchema, getAllPatientVisitsQuerySchema, patientVisitDetailParamScheme } from './patient-visit.scheme'

const patientVisitRoute = Router()

patientVisitRoute.route("/")
    .get([validateQuery(getAllPatientVisitsQuerySchema)], getAllPatientVisitsHandler)
    .post([validateBody(createPatientVisitBodySchema)], createPatientVisitHandler)

patientVisitRoute.route("/:visit").all([validateParam(patientVisitDetailParamScheme)])
    .get(getPatientVisitHandler)
    .delete(deletePatientVisitHandler)

patientVisitRoute.post("/:visit/restore", [validateParam(patientVisitDetailParamScheme)], restorePatientVisitHandler)

export default patientVisitRoute

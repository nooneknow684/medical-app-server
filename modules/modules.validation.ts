import prisma from "../utils/prisma";



export const patientExists = {
    validator: (deleted: boolean = false) => {
        return async (patient_id: number) => (await prisma.patient.count({ where: { id: patient_id, deleted_at: deleted ? { not: null } : null } })) > 0
    },
    message: (result: number) => ({ message: `no patient unit with id ${result}` })
}

export const medicineUnitExits = {
    validator: (deleted: boolean = false) => {
        return async (unit_id: number) => (await prisma.medicineUnit.count({ where: { id: unit_id, deleted_at: deleted ? { not: null } : null } })) > 0
    },
    message: (result: number) => ({ message: `no medicine unit with id ${result}` })
}

export const medicineExists = {
    validator: (deleted: boolean = false) => {
        return async (unit_id: number) => (await prisma.medicine.count({ where: { id: unit_id, deleted_at: deleted ? { not: null } : null } })) > 0
    },
    message: (result: any) => ({ message: `no medicine with id ${result}` })
}

export const patientVisitExists = {
    validator: (deleted: boolean = false) => {
        return async (visit_id: number) => (await prisma.patientVisit.count({ where: { id: visit_id, deleted_at: deleted ? { not: null } : null } })) > 0
    },
    message: (result: any) => ({ message: `no patient visits with id ${result}` })
}

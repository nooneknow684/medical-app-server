import prisma from "../utils/prisma"

export const clearDatabase = async () => {
    await prisma.patientVisitMedicine.deleteMany()
    await prisma.patientVisit.deleteMany()
    await prisma.patient.deleteMany()
    await prisma.medicineDetail.deleteMany()
    await prisma.medicine.deleteMany()
    await prisma.medicineUnit.deleteMany()
}

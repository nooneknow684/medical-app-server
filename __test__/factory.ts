import prisma from "../utils/prisma"

import patientMock from './patient.json'
import medicineMock from './medicine.json'

export const medicineUnitFactory = async (names: { id?: number, name: string, deleted_at?: Date }[]) => {
    return await prisma.medicineUnit.createMany({
        data: names.map((e) => ({ id: e.id, name: e.name, deleted_at: e.deleted_at }))
    })
}
export const medicineFactory = async (medicines: { id?: number, name: string, deleted_at?: Date }[]) => {
    return await prisma.medicine.createMany({
        data: medicines.map((e) => ({ id: e.id, name: e.name, deleted_at: e.deleted_at }))
    })
}

export const medicineDetailFactory = async (data: { medicine_id: number, unit_id: number, qty: number }[]) => {
    return await prisma.medicineDetail.createMany({
        data: data.map(({ medicine_id, unit_id, qty }) => ({
            medicine_id,
            unit_id,
            qty
        }))
    })
}

export const patientFactory = async (data: { id?: number, name: string, no_ktp: string, birthdate: string, sex: "male" | "female", sickness?: string[], deleted_at?: Date }[]) => {
    return await prisma.patient.createMany({
        data: data.map((e) => ({
            id: e.id,
            name: e.name,
            no_ktp: e.no_ktp,
            birthdate: new Date(e.birthdate),
            sex: e.sex,
            sickness: e.sickness ?? [],
            deleted_at: e.deleted_at
        }))
    })
}

export const mockPatient = async () => {
    return await patientFactory(patientMock.map(({ sex, ...rest }, index) => ({ ...rest, sex: sex == "male" ? "male" : "female", deleted_at: index >= 87 ? new Date() : undefined })))
}

export const mockMedicine = async () => {
    return await medicineFactory(medicineMock.map((data, index) => ({ ...data, deleted_at: index >= 87 ? new Date() : undefined })))
}

/*
  Warnings:

  - You are about to drop the `PatientSickness` table. If the table is not empty, all the data it contains will be lost.
  - You are about to drop the `PatientVisitSymptom` table. If the table is not empty, all the data it contains will be lost.
  - Added the required column `sickness` to the `Patient` table without a default value. This is not possible if the table is not empty.
  - Added the required column `symtoms` to the `PatientVisit` table without a default value. This is not possible if the table is not empty.

*/
-- DropForeignKey
ALTER TABLE `PatientSickness` DROP FOREIGN KEY `PatientSickness_patient_id_fkey`;

-- DropForeignKey
ALTER TABLE `PatientVisitSymptom` DROP FOREIGN KEY `PatientVisitSymptom_visit_id_fkey`;

-- AlterTable
ALTER TABLE `Patient` ADD COLUMN `sickness` JSON NOT NULL;

-- AlterTable
ALTER TABLE `PatientVisit` ADD COLUMN `symtoms` JSON NOT NULL;

-- DropTable
DROP TABLE `PatientSickness`;

-- DropTable
DROP TABLE `PatientVisitSymptom`;

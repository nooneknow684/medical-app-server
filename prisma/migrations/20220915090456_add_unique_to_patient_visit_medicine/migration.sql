/*
  Warnings:

  - A unique constraint covering the columns `[medicine_detail_id,visit_id]` on the table `PatientVisitMedicine` will be added. If there are existing duplicate values, this will fail.

*/
-- CreateIndex
CREATE UNIQUE INDEX `PatientVisitMedicine_medicine_detail_id_visit_id_key` ON `PatientVisitMedicine`(`medicine_detail_id`, `visit_id`);

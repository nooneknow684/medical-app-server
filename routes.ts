import { Router } from 'express'
import medicineDetailRoutes from './modules/medicine-detail/medicine-detail.routes'
import medicineUnitRoute from './modules/medicine-unit/medicine-unit.routes'
import medicineRoute from './modules/medicine/medicine.routes'
import patientVisitRoute from './modules/patient-visit/patient-visit.route'
import patientRoute from './modules/patient/patient.routes'

const router = Router()

router.get('/healthcheck', (_, res) => res.status(200).json({ status: "OK" }))
router.use('/unit', medicineUnitRoute)
router.use('/medicine', medicineRoute)
router.use('/medicine', medicineDetailRoutes)
router.use('/patient', patientRoute)
router.use('/visit', patientVisitRoute)

router.get('/test', (req, res) => {
    return res.json(req.query)
})
export default router

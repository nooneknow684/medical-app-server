import { PrismaClient } from "@prisma/client"
import { createPrismaQueryEventHandler } from 'prisma-query-log';
import config from "./config";

const prisma = new PrismaClient({
    datasources: {
        db: {
            url: config.DATABASE_URL
        }
    },
    log: [{
        level: 'query',
        emit: 'event'
    }]
})
// prisma.$on('query', createPrismaQueryEventHandler({
//     unescape: false,
//     queryDuration: true,
//     language: 'sql',
//     indent: '  ',
//     linesBetweenQueries: 2
// }))

export default prisma

import { z } from 'zod'

const envScheme = z.object({
    DATABASE_URL: z.string(),
    PORT: z.preprocess(e => parseInt(String(e)), z.number()),
    HOST: z.string()

})
export default envScheme.parse(process.env)

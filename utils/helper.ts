import { NextFunction, Request, Response } from "express";
import { Query, Send, ParamsDictionary } from "express-serve-static-core"
import { Refinement, SuperRefinement, z, ZodAny, ZodError, ZodSchema } from "zod";

export type TypedRequest<TBody = any, TParam = any, TQuery = Query> = Request<TParam & ParamsDictionary, any, TBody, TQuery>
export interface TypedResponse<ResBody> extends Response { json: Send<ResBody, this>; }

export function validateBody(bodySchema: ZodSchema) {
    return async (req: Request, res: Response, next: NextFunction) => {
        const result = await z.object({ body: bodySchema }).parseAsync({ body: req.body })
        req.body = result.body
        return next()
    }
}

export function validateParam(paramSchema: ZodSchema) {
    return async (req: Request, res: Response, next: NextFunction) => {
        try {
            const result = await z.object({ param: paramSchema }).parseAsync({ param: req.params })
            req.params = result.param
            return next()

        } catch (error) {
            if (error instanceof ZodError) return res.status(404).json({
                error: {
                    code: 404,
                    message: "NotFound",
                    issues: error.issues
                }
            })
            return next(error)
        }
    }
}

export function validateQuery(querySchema: ZodSchema) {
    return async (req: Request, res: Response, next: NextFunction) => {
        const result = await z.object({ query: querySchema }).parseAsync({ query: req.query })
        req.query = result.query
        return next()
    }
}

export function paginateResponse<T>(query: { page: number, per_page: number }, count: number, data: Array<T>) {
    return {
        status: 200,
        pagination: {
            page: query.page,
            per_page: query.per_page,
        },
        count,
        data
    }

}

type SortCheckerReturn = 'asc' | 'desc' | undefined


export function sortChekcer(field_name: string, value: string): SortCheckerReturn {
    return value === field_name ? 'asc' : value === `-${field_name}` ? 'desc' : undefined
}

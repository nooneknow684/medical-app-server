import express_winston from 'express-winston'
import winston from 'winston'

export const consoleLogger = express_winston.logger({
    transports: [new winston.transports.Console()],
    format: winston.format.combine(
        winston.format.colorize(),
        winston.format.cli()
    ),
    meta: true,
    expressFormat:true
})

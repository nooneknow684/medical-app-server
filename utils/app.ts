import express from 'express'
import "express-async-errors"

import cors from 'cors'
import { json } from 'body-parser'

import { consoleLogger } from './logger'
import routes from '../routes'
import errorHandler from './error-handler'

const createApp = () => {
    const app = express()

    app.use(cors(), json())
        .use(consoleLogger)
        .use('/', routes)
        .use(errorHandler)

    return app
}

export default createApp

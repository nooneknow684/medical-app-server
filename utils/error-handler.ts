import { PrismaClientKnownRequestError } from '@prisma/client/runtime'
import { Request, Response, NextFunction } from 'express'
import { HttpError } from 'http-errors'
import { ZodError } from 'zod'
const httpErrorHandler = (error: Error, _req: Request, res: Response, next: NextFunction) => {
    if (!(error instanceof HttpError)) return next(error)
    return res.status(error.status).json({
        error: {
            message: error.message,
            name: error.name,
            code: error.statusCode,
        }
    })
}

const prismaErrorHandler = (error: Error, _req: Request, res: Response, next: NextFunction) => {
    if (!(error instanceof PrismaClientKnownRequestError)) return next(error)
    return res.status(500).json({
        error: {
            message: error.message.split("\n") ?? [],
            code: 500,
            target: error.meta,
            prisma_code: error.code,
            stack: error.stack?.split("\n") ?? []
        }
    })
}

const zodErrorHandler = (error: Error, _req: Request, res: Response, next: NextFunction) => {
    if (!(error instanceof ZodError)) return next(error)
    return res.status(400).json({
        error: {
            code: 400,
            message: "BadRequest",
            issues: error.issues
        }
    })
}

const fallbackErrorHandler = (error: Error, _req: Request, res: Response, _next: NextFunction) => {
    return res.status(500).json({
        error: {
            message: error.message.split("\n"),
            code: 500,
            stack: error.stack?.split("\n") ?? []
        }
    })
}

export default [zodErrorHandler, httpErrorHandler, prismaErrorHandler, fallbackErrorHandler]
